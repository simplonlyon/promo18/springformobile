# Api rest pour mobile
Une petite api rest qui permettra de stocker la position de trucs qui sera utiliser par une application mobile.

## How to use
1. Créer une base de données (par exemple `p18_mobile`)
2. Importer le  [fichier sql](src/main/resources/database.sql) sur cette base de données
3. Lancer l'application