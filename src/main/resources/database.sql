DROP TABLE IF EXISTS location;
DROP TABLE IF EXISTS tracked;

CREATE TABLE tracked(
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(128) NOT NULL,
    image VARCHAR(256)
);

CREATE TABLE location(
    id INT PRIMARY KEY AUTO_INCREMENT,
    timestamp INT NOT NULL,
    latitude DOUBLE NOT NULL,
    longitude DOUBLE NOT NULL,
    tracked_id INT,
    FOREIGN KEY (tracked_id) REFERENCES tracked(id) ON DELETE CASCADE
);

INSERT INTO tracked (label, image) VALUES ('Corgi', 'https://cdn.pixabay.com/photo/2019/08/19/07/45/corgi-4415649_960_720.jpg');
INSERT INTO tracked (label, image) VALUES ('Pumpkins', 'https://cdn.pixabay.com/photo/2018/08/27/23/30/pumpkins-3636243_960_720.jpg');

INSERT INTO location (timestamp, latitude,longitude, tracked_id) VALUES (1658393672, 45.7561351, 4.894785, 1);
INSERT INTO location (timestamp, latitude,longitude, tracked_id) VALUES (1658394000, 45.75713031390088, 4.902058873441616, 1);
INSERT INTO location (timestamp, latitude,longitude, tracked_id) VALUES (1658396000, 45.7568876201613, 4.895657229245069, 2);