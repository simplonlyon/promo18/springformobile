package co.simplon.promo18.springformobile.entity;

import java.util.ArrayList;
import java.util.List;

public class Tracked {
    private Integer id;
    private String label;
    private String image;
    //Potentiellement inutile
    private List<Location> locations = new ArrayList<>();
    public Tracked() {
    }
    public Tracked(String label, String image) {
        this.label = label;
        this.image = image;
    }
    public Tracked(Integer id, String label, String image) {
        this.id = id;
        this.label = label;
        this.image = image;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public List<Location> getLocations() {
        return locations;
    }
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
