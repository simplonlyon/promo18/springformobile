package co.simplon.promo18.springformobile.entity;

public class Location {
    private Integer id;
    private Integer timestamp;
    private Double latitude;
    private Double longitude;
    private Tracked tracked;
    public Tracked getTracked() {
        return tracked;
    }
    public void setTracked(Tracked tracked) {
        this.tracked = tracked;
    }
    public Location() {
    }
    public Location(Integer timestamp, Double latitude, Double longitude) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public Location(Integer id, Integer timestamp, Double latitude, Double longitude) {
        this.id = id;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
