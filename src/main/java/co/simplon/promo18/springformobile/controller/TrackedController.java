package co.simplon.promo18.springformobile.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springformobile.entity.Tracked;
import co.simplon.promo18.springformobile.repository.TrackedRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/tracked")
public class TrackedController {
    @Autowired
    private TrackedRepository repo;

    @GetMapping
    public List<Tracked> getAll() {
        return repo.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Tracked add(@RequestBody Tracked tracked) {
        String filename = UUID.randomUUID() + ".jpg";
        byte[] decoded = Base64.getDecoder().decode(tracked.getImage());
        
        try(OutputStream writer = new FileOutputStream(new File(getUploadFolder(),filename))) {
            writer.write(decoded);
            
            tracked.setImage(filename);
            repo.save(tracked);

        } catch (IllegalStateException | IOException e) {
            
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "upload error");
        }

        return tracked;
    }

    private File getUploadFolder() {
        File folder =
            new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
          folder.mkdirs();
        }
        return folder;
    
      }

}
