package co.simplon.promo18.springformobile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.springformobile.entity.Location;
import co.simplon.promo18.springformobile.repository.LocationRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/location")
public class LocationController {
    @Autowired
    private LocationRepository repo;

    @GetMapping
    public List<Location> getLatest() {
        return repo.findLatests();
    }

    @GetMapping("/tracked/{id}")
    public List<Location> getByTracked(@PathVariable int id) {
        return repo.findByTracked(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Location add(@RequestBody Location location) {

        repo.save(location);
        return location;
    }


}
