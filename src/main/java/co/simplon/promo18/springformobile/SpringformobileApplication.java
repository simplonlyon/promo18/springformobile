package co.simplon.promo18.springformobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringformobileApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringformobileApplication.class, args);
	}

}
