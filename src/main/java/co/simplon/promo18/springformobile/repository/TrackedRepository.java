package co.simplon.promo18.springformobile.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springformobile.entity.Tracked;

@Repository
public class TrackedRepository {
    @Autowired
    private DataSource dataSource;

    public List<Tracked> findAll() {
        List<Tracked> list = new ArrayList<>();
        try (Connection cnx = dataSource.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM tracked");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Tracked tracked = new Tracked(
                        rs.getInt("id"),
                        rs.getString("label"),
                        rs.getString("image"));
                list.add(tracked);

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database error");
        }
        return list;
    }

    public void save(Tracked tracked) {

        try (Connection cnx = dataSource.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("INSERT INTO tracked (label, image) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, tracked.getLabel());
            stmt.setString(2, tracked.getImage());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {
                tracked.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database error");
        }
    }

}
