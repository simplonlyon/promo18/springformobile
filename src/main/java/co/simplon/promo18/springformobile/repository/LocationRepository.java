package co.simplon.promo18.springformobile.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springformobile.entity.Location;
import co.simplon.promo18.springformobile.entity.Tracked;

@Repository
public class LocationRepository {
    @Autowired
    private DataSource dataSource;

    public List<Location> findLatests() {
        List<Location> list = new ArrayList<>();
        try (Connection cnx = dataSource.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("""
            SELECT a.*, t.* FROM location a INNER JOIN
            (SELECT tracked_id, MAX(timestamp) as maxtime FROM location GROUP BY tracked_id) b
            ON a.tracked_id=b.tracked_id AND a.timestamp=b.maxtime INNER JOIN tracked t ON a.tracked_id=t.id
            """);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Location location = new Location(
                    rs.getInt("id"),
                    rs.getInt("timestamp"),
                    rs.getDouble("latitude"),
                    rs.getDouble("longitude")
                );

                Tracked tracked = new Tracked(
                        rs.getInt("tracked_id"),
                        rs.getString("label"),
                        rs.getString("image"));
                location.setTracked(tracked);

                list.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database error");
        }
        return list;
    }

    public List<Location> findByTracked(int trackedId) {
        List<Location> list = new ArrayList<>();
        try (Connection cnx = dataSource.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM location WHERE tracked_id=?");
            stmt.setInt(1, trackedId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Location location = new Location(
                    rs.getInt("id"),
                    rs.getInt("timestamp"),
                    rs.getDouble("latitude"),
                    rs.getDouble("longitude")
                );
                list.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database error");
        }
        return list;
    }

    public void save(Location location) {

        try (Connection cnx = dataSource.getConnection()) {
            PreparedStatement stmt = cnx.prepareStatement("INSERT INTO location (timestamp, latitude,longitude,tracked_id) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, location.getTimestamp());
            stmt.setDouble(2, location.getLatitude());
            stmt.setDouble(3, location.getLongitude());
            stmt.setInt(4, location.getTracked().getId());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {
                location.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database error");
        }
    }
}
