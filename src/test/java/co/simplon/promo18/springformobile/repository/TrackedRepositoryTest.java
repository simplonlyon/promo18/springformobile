package co.simplon.promo18.springformobile.repository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplon.promo18.springformobile.entity.Tracked;

@SpringBootTest
@Sql({"/database.sql"})
public class TrackedRepositoryTest {

    @Autowired
    private TrackedRepository repo;

    @Test
    void testFindAll() {
        List<Tracked> result = repo.findAll();

        assertThat(result).hasSize(2)
        .doesNotContainNull()
        .allSatisfy(tracked -> 
            assertThat(tracked).hasNoNullFieldsOrProperties()
        );


    }

    @Test
    void testSave() {
        Tracked toSave = new Tracked("test", "testimg");

        repo.save(toSave);

        assertThat(toSave.getId()).isNotNull();
    }
}
