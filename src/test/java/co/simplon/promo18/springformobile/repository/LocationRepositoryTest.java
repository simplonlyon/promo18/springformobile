package co.simplon.promo18.springformobile.repository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplon.promo18.springformobile.entity.Location;
import co.simplon.promo18.springformobile.entity.Tracked;

@SpringBootTest
@Sql({ "/database.sql" })
public class LocationRepositoryTest {
    @Autowired
    private LocationRepository repo;

    @Test
    void testFindLatests() {
        List<Location> result = repo.findLatests();

        assertThat(result).hasSize(2)
                .doesNotContainNull()
                .allSatisfy(location -> {
                    assertThat(location).hasNoNullFieldsOrProperties();
                    assertThat(location.getTracked()).hasNoNullFieldsOrProperties();
                });
        //Plus précis, moins maintenable, mais test qu'on a bien le résultat attendu
        assertThat(result.get(0).getTimestamp()).isEqualTo(1658394000);
    }
    @Test
    void testFindByTracked() {
        List<Location> result = repo.findByTracked(1);

        assertThat(result).hasSize(2)
                .doesNotContainNull()
                .allSatisfy(location ->
                    assertThat(location).hasNoNullFieldsOrPropertiesExcept("tracked")
                );
       
    }

    @Test
    void testSave() {
        Location toSave = new Location(10000, 10.123123, 10.6534452);
        toSave.setTracked(new Tracked(1, null, null));

        repo.save(toSave);

        assertThat(toSave.getId()).isNotNull();
    }
}
